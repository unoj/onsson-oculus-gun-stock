# Oculus Gun Stock

Files for the Oculus gun stock project as described on http://www.onsson.com/posts/projects/2019/making-a-3d-printed-oculus-gun-stock/

Two versions of the stock 

Mark I attaches to the top of the controller.

Mark II is more stable and better in almost every aspect.