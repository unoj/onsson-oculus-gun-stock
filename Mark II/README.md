# Oculus Gun Stock Mark II

Files for the Oculus gun stock project as described in post on http://www.onsson.com/posts/projects/2019/making-a-3d-printed-oculus-gun-stock/

Upgraded to fit both CV1 and S.

### BOM

* Aluminium pipe 16mm
    * 280mm x 1
    * 120mm x 1
* 4mm bolts
    * 40mm x 1
    * 30mm x 3
    * 25mm x 1
* 4mm locknuts x 4
* 4mm wingnut x 1
* 8mm washers x 8